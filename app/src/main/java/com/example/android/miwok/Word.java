package com.example.android.miwok;

public class Word {

    private String miwok, translated;
    private int imgResId = isThereImg;
    private static final int isThereImg = -1;
    private int audioId;

    public Word(String miwok, String translated, int audioId) {
        this.miwok = miwok;
        this.translated = translated;
        this.audioId = audioId;
    }

    public Word(String miwok, String translated, int imgResId, int audioId) {
        this.miwok = miwok;
        this.translated = translated;
        this.imgResId = imgResId;
        this.audioId = audioId;
    }

    public String getMiw(){
        return this.miwok;
    }

    public String getTranslated(){
        return this.translated;
    }

    public int getImgResId(){ return this.imgResId; }

    public int getAudioId() { return this.audioId; }

    public boolean hasImg() {
        return this.imgResId != isThereImg;
    }
}
