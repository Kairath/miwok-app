package com.example.android.miwok;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RemoteControlClient;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class NumbersActivity extends AppCompatActivity {

    private MediaPlayer media;
    private AudioManager mAudioManager;

    AudioManager.OnAudioFocusChangeListener mOnAudioChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                            focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                        media.pause();
                        media.seekTo(0);
                    } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                        media.start();
                    } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                        releaseMediaPlayer();
                    }
                }
            };

    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            releaseMediaPlayer();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        final ArrayList<Word> wordList = new ArrayList<Word>();

        wordList.add(new Word("one", "zzz", R.drawable.number_one, R.raw.number_one));
        wordList.add(new Word("two", "lutti", R.drawable.number_two, R.raw.number_two));
        wordList.add(new Word("three", "otiiko", R.drawable.number_three, R.raw.number_three));
        wordList.add(new Word("four", "tolookosu", R.drawable.number_four, R.raw.number_four));
        wordList.add(new Word("five", "massoka", R.drawable.number_five, R.raw.number_five));
        wordList.add(new Word("six", "temmoka", R.drawable.number_six, R.raw.number_six));
        wordList.add(new Word("seven", "kenekaku", R.drawable.number_seven, R.raw.number_seven));
        wordList.add(new Word("eight", "kawenta", R.drawable.number_eight, R.raw.number_eight));
        wordList.add(new Word("nine", "woe", R.drawable.number_nine, R.raw.number_nine));
        wordList.add(new Word("ten", "na acha", R.drawable.number_ten, R.raw.number_ten));

        WordAdapter adapter = new WordAdapter(this, wordList, R.color.category_numbers);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Word currentWord = wordList.get(i);

                //cheking if already a running one if so removes from memory
                releaseMediaPlayer();


                //Audio Manager stuff
                int result = mAudioManager.requestAudioFocus(mOnAudioChangeListener,
                        AudioManager.STREAM_MUSIC,
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                if(result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){

                    media = MediaPlayer.create(NumbersActivity.this, currentWord.getAudioId());
                    media.start();

                    //on clck if already playing removes from memory
                    media.setOnCompletionListener(mCompletionListener);
                }


            }
        });
    }


    //when app paused clear the memory-music stopes
    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    private void releaseMediaPlayer() {
        // If the media player is not null, then it may be currently playing a sound.
        if (media != null) {
            // Regardless of the current state of the media player, release its resources
            // because we no longer need it.
            media.release();

            // Set the media player back to null. For our code, we've decided that
            // setting the media player to null is an easy way to tell that the media player
            // is not configured to play an audio file at the moment.
            media = null;

            mAudioManager.abandonAudioFocus(mOnAudioChangeListener);
        }
    }
}
