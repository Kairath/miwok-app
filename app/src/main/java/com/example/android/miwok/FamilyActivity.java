package com.example.android.miwok;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class FamilyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);

        final ArrayList<Word> wordList = new ArrayList<Word>();

        wordList.add(new Word("father", "zzz", R.drawable.family_father, R.raw.family_father));
        wordList.add(new Word("mother", "lutti", R.drawable.family_mother, R.raw.family_mother));
        wordList.add(new Word("grand father", "otiiko", R.drawable.family_grandfather, R.raw.family_grandfather));
        wordList.add(new Word("graned mother", "tolookosu", R.drawable.family_grandmother, R.raw.family_grandmother));
        wordList.add(new Word("son", "massoka", R.drawable.family_son, R.raw.family_son));
        wordList.add(new Word("older brother", "temmoka", R.drawable.family_older_brother, R.raw.family_older_brother));
        wordList.add(new Word("older sister", "kenekaku", R.drawable.family_older_sister, R.raw.family_older_sister));
        wordList.add(new Word("younger brother", "kawenta", R.drawable.family_younger_brother, R.raw.family_younger_brother));
        wordList.add(new Word("younger sister", "woe", R.drawable.family_younger_sister, R.raw.family_younger_sister));
        wordList.add(new Word("daughter", "na acha", R.drawable.family_daughter, R.raw.family_daughter));

        WordAdapter adapter = new WordAdapter(this, wordList, R.color.category_family);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Word currentWord = wordList.get(i);

                MediaPlayer media = MediaPlayer.create(FamilyActivity.this, currentWord.getAudioId());
                media.start();
            }
        });

    }
}
