package com.example.android.miwok;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class PhrasesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);

        final ArrayList<Word> wordList = new ArrayList<Word>();

        wordList.add(new Word("are you coming", "family", R.raw.phrase_are_you_coming));
        wordList.add(new Word("come here", "lutti", R.raw.phrase_come_here));
        wordList.add(new Word("how are you feeling", "otiiko", R.raw.phrase_how_are_you_feeling));
        wordList.add(new Word("im coming", "tolookosu", R.raw.phrase_im_coming));
        wordList.add(new Word("im feeling good", "massoka", R.raw.phrase_im_feeling_good));
        wordList.add(new Word("lets go", "temmoka", R.raw.phrase_lets_go));
        wordList.add(new Word("my name is", "kenekaku", R.raw.phrase_my_name_is));
        wordList.add(new Word("what is your name", "kawenta", R.raw.phrase_what_is_your_name));
        wordList.add(new Word("where are you going", "woe", R.raw.phrase_where_are_you_going));
        wordList.add(new Word("yes im coming", "na acha", R.raw.phrase_yes_im_coming));

        WordAdapter adapter = new WordAdapter(this, wordList, R.color.category_phrases);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Word currentWord = wordList.get(i);

                MediaPlayer media = MediaPlayer.create(PhrasesActivity.this, currentWord.getAudioId());
                media.start();
            }
        });

    }
}
