package com.example.android.miwok;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static android.view.View.INVISIBLE;

/**
 * Created by eko on 18.07.2016.
 */
public class WordAdapter extends ArrayAdapter<Word> {

    private int colorId;

    public WordAdapter(Activity context, ArrayList<Word> wordList, int color){
        super(context, 0, wordList);
        this.colorId = color;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        // Get the {@link AndroidFlavor} object located at this position in the list
        Word currentWord = getItem(position);

        // Find the TextView in the list_item.xml layout with the ID version_name
        TextView nameTextView = (TextView) listItemView.findViewById(R.id.miwok_one);
        // Get the version name from the current AndroidFlavor object and
        // set this text on the name TextView
        nameTextView.setText(currentWord.getMiw());

        // Find the TextView in the list_item.xml layout with the ID version_number
        TextView numberTextView = (TextView) listItemView.findViewById(R.id.translated_one);
        // Get the version number from the current AndroidFlavor object and
        // set this text on the number TextView
        numberTextView.setText(currentWord.getTranslated());

        ImageView imageOfItem = (ImageView) listItemView.findViewById(R.id.item_image);



        if( currentWord.hasImg() ) {
            imageOfItem.setImageResource(currentWord.getImgResId());
        } else {
            imageOfItem.setVisibility(View.GONE);
        }

        View textContainer =  listItemView.findViewById(R.id.text_container);
        View button = listItemView.findViewById(R.id.play_button);
        int color = ContextCompat.getColor(getContext(), colorId);
        textContainer.setBackgroundColor(color);
        button.setBackgroundColor(color);

        // Return the whole list item layout (containing 2 TextViews and an ImageView)
        // so that it can be shown in the ListView
        return listItemView;
    }
}
