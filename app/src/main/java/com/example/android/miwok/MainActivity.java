/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.miwok;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the content of the activity to use the activity_main.xml layout file
        setContentView(R.layout.activity_main);

        TextView familyTextView = (TextView) findViewById(R.id.family);

        familyTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Toast t = Toast.makeText(getApplicationContext(), "this one uses onClickListener", Toast.LENGTH_SHORT);
                t.show();

                Intent i = new Intent(MainActivity.this, FamilyActivity.class);
                startActivity(i);
            }

        });
    }

    public void numbersClickHandler(View view){
        Toast t = Toast.makeText(getApplicationContext(), "changing acitiviy via intent", Toast.LENGTH_SHORT);
        t.show();
        Intent i = new Intent(this, NumbersActivity.class);
        startActivity(i);
    }


    public void colorsClickHandler(View view){
        Intent i = new Intent(this, ColorsActivity.class);
        startActivity(i);
    }

    public void phrasesClickHandler(View view){
        Intent i = new Intent(this, PhrasesActivity.class);
        startActivity(i);
    }

    public void mainClickHandler(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }


}
