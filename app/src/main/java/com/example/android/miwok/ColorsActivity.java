package com.example.android.miwok;

import android.media.MediaPlayer;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ColorsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);

        final ArrayList<Word> wordList = new ArrayList<Word>();

        wordList.add(new Word("black", "zzz", R.drawable.color_black, R.raw.color_black));
        wordList.add(new Word("brown", "lutti", R.drawable.color_brown, R.raw.color_brown));
        wordList.add(new Word("grey", "tolookosu", R.drawable.color_gray, R.raw.color_gray));
        wordList.add(new Word("green", "massoka", R.drawable.color_green, R.raw.color_green));
        wordList.add(new Word("yellow", "temmoka", R.drawable.color_mustard_yellow, R.raw.color_mustard_yellow));
        wordList.add(new Word("red", "kenekaku", R.drawable.color_red, R.raw.color_red));
        wordList.add(new Word("dusty yellow", "woe", R.drawable.color_dusty_yellow, R.raw.color_dusty_yellow));
        wordList.add(new Word("white", "na acha", R.drawable.color_white, R.raw.color_white));

        WordAdapter adapter = new WordAdapter(this, wordList, R.color.category_colors);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Word currentWord = wordList.get(i);

                MediaPlayer media = MediaPlayer.create(ColorsActivity.this, currentWord.getAudioId());
                media.start();
            }
        });
    }
}
